FROM registry.gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin:17-jre-alpine-opentelemetry

USER root

# Create user to comply to numeric UID for k8s. This workaround should be adopted in the base image.
RUN addgroup consumer && adduser -S -h /home/consumer -G consumer -u 1001 consumer && \
  chown -R consumer:consumer /opt/app && \
  chown -h consumer:consumer /home/consumer

USER 1001

COPY --chown=consumer:consumer target/*.jar /opt/app/app.jar

EXPOSE 8080
ENTRYPOINT ["java","-javaagent:opentelemetry-javaagent.jar","-XX:MaxRAMPercentage=70","-jar","/opt/app/app.jar"]
