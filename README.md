# JMS Consumer & OSDBK

The JMS Consumer is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of seven applications:  

- EbMS Core / EbMS Admin
- JMS Producer  
- JMS Consumer  
- Apache ActiveMQ
- CPA Service
- OSDBK Admin Console
- Throttling Service (optional)

The JMS Consumer is responsible for dispatching messages from ActiveMQ queues to EbMS Core for the outbound EbMS flow.  

On receiving a message from ActiveMQ, the JMS Consumer sends the content of some JMS headers (see below) of the message to the CPA Service. The CPA Service will try to find the corresponding CPA and will send the required CPA parameters in the response. 
The JMS Consumer then proceeds to send the message via the REST Message Service of EbMS Core, which in turn will send the message to its destination.
Optionally, when throttling is enabled, before the message is sent to EbMS Core, the JMS Consumer will make a REST call to the Throttling Service to verify if the destination endpoint is able to receive messages within the configured throttle value.

The JMS Consumer also exposes two REST endpoints, that can be used to resend FAILED and EXPIRED messages.The REST API is protected via Basic Authentication.

### ActiveMQ Configuration  
The JMS Consumer implements the Spring Boot preconfigured ActiveMQ Listener with XA-transactions enabled. URL and credentials can be configured in the application.yml.  

~~~
spring:
  activemq:
    broker-url: tcp://activemq:61616
    user: jmsconsumer
    password: jmsconsumer
~~~

### Queue Listener Configuration  
The JMS Consumer implements a JMS Message Listener that can be configured to listen to an X amount of queues:
~~~
jms-consumer:  
  listeners:  
    - queueName: "Small_Afnemer_InQueue"  
      concurrency: "2-10" 
      oinFilter: "11111111111111111111,22222222222222222222" 
    - queueName: "Big_Afnemer_InQueue"  
      concurrency: "3"  
~~~
The concurrency determines the amount of listeners that will be used for a specific queue  

Optionally you can configure a comma separated list of OIN's that should NOT be processed and read from the queue
during a maintenance window, by making use of the 'oinFilter' property.
The OIN is stored in the 'x_aux_receiver_id' property in the JMS Header of the message.

### EbMS Core REST Service Configuration  
EbMS Core exposes its REST Services by default on port 8888. If needed, the endpoints can be configured differently:  
~~~
ebms:  
    message-service:  
        url: "http://ebms-core:8888/service/ebms"  
~~~

### CPA Service REST Configuration  
The CPA Service exposes its REST endpoint by default on port 8080. If needed, the endpoints can be configured differently:  
~~~
cpa-service:  
    url: "http://cpa-service:8080/cpa/find"  
~~~

### Throttling Configuration
The JMS Consumer can be configured to use throttling via the Throttling Service application.
Throttling can be enabled by setting the throttling.enabled property to true.
~~~
throttling:
  enabled: true
  url: "http://throttling-service:8080/throttling"
~~~

### Resend Configuration
By default, EbMS Core will add a message event on the FAILED or EXPIRED queue for messages that can not be delivered to a receiver.
This can be due to network problems or technical problems experienced by the receiver.

The JMS Consumer features two endpoints that are used by the OSDBK Admin Console to resend these messages. These endpoints are protected 
by basic authentication and are not exposed to other applications. Basic Authentication can be configured via:
~~~
spring:
  security:
    user:
      name: changeit
      password: changeit
~~~

The JMS Consumer adds a JMS Listener to the default FAILED and EXPIRED queues of EbMS Core. If needed, the names can be adjusted.
~~~
resend:
  queue:
    failed: "FAILED"
    expired: "EXPIRED"
~~~ 

### JMS Header Specification
A JMS message that is sent to the queue that is listened to by the JMS Consumer, MUST have the JMS headers set as are mentioned in the table below. The JMS Consumer uses the x_aux_action, x_aux_activity, x_aux_process_type, x_aux_receiver_id and x_aux_sender_id to find a matching CPA. The x_aux_system_msg_id is used as the messageId for the outgoing message.

| Name | Description | Max Length | Content |
|---|---|---|--|
| x_aux _action |	The CPA action |	100 |	verstrekkingAanAfnemer  |
| x_aux_activity |  The CPA service |    100 |    dgl:ontvangen:1.0  |
|x_aux_process_type|The CPA service|100|dgl:ontvangen:1.0|
|x_aux_process_version|The CPA version|100|1.0|
|x_aux_production|Mandatory Production Flag|100|Production|
|x_aux_protocol|The communication protocol|100|ebMS|
|x_aux_protocol_version|The communication protocol version|100|2.0|
|x_aux_receiver_id| The OIN of the receiving CPA Party|24|12345678901234567890 (example)|
|x_aux_sender_id|The OIN of the sending CPA Party|24|00000004003214345001 (example)|
|x_aux_system_msg_id|UUID of the message|255|UUID|
|x_aux_process_instance_id|UUID of the conversation|255|UUID|
|x_aux_seq_number|The sequence number used for message ordering feature. Default 0|255|0|
|x_aux_msg_order|Indicates if parties support message ordering. Default false|100|false|

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However, the following variables need to be populated by means of a `.env` file:
~~~
${OSDBK_IMAGE_REPOSITORY} -- location of your Docker repository where you are housing your built OSDBK docker images
${ACTIVEMQ_IMAGE_TAG} -- the tag of your built ActiveMQ docker image
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
${DGL_STUB_IMAGE_TAG}  -- the tag of your built stub application docker image, to simulate interactions with an external party
${DGL_STUB_MESSAGE_SENDER_OIN_AFNEMER} -- the OIN of your stubbed application
${EBMS_CORE_IMAGE_TAG} -- the tag of your built ebms-core docker image
${EBMS_JDBC_USERNAME} -- the username used to connect to the EBMS database
${EBMS_JDBC_PASSWORD} -- the password used to connect to the EBMS database
${EBMS_JMS_BROKER_USERNAME} -- the username used to connect the ebms application to the ActiveMQ instance
${EBMS_JMS_BROKER_PASSWORD} -- the password used to connect the ebms application to the ActiveMQ instance
${EBMS_POSTGRESQL_IMAGE_TAG} -- the tag of your built EBMS PostgresQL docker image
${EBMS_STUB_IMAGE_TAG} -- the tag of your built stub application docker image, to simulate interactions with an external party
${JMS_CONSUMER_ACTIVEMQ_USER} -- the username used to connect the jms-consumer application to the ActiveMQ instance
${JMS_CONSUMER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-consumer application to the ActiveMQ instance
${JMS_PRODUCER_IMAGE_TAG} -- the tag of your built jms-producer docker image
${JMS_PRODUCER_ACTIVEMQ_USER} -- the username used to connect the jms-producer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-producer application to the ActiveMQ instance
${THROTTLING_ENABLED} -- a boolean flag indicating whether throttling is enabled or disabled
${THROTTLING_URL} -- the endpoint of the throttling service to be used
${THROTTLING_SERVICE_IMAGE_TAG} -- the tag of your built throttling-service docker image
${CPA_SERVICE_IMAGE_TAG} -- the tag of your built cpa-service docker image
${CPA_JDBC_USERNAME} the username used to connect to the CPA database
${CPA_JDBC_PASSWORD} the password used to connect to the CPA database
${CPA_SERVICE_ACTIVEMQ_USER} -- the username used to connect the cpa-service application to the ActiveMQ instance
${CPA_SERVICE_ACTIVEMQ_PASSWORD} -- the password used to connect the cpa-service application to the ActiveMQ instance
${CPA_DATABASE_IMAGE_TAG} -- the tag of your built CPA PostgresQL docker image
${OSDBK_ADMIN_CONSOLE_IMAGE_TAG} -- the tag of your built OSDBK Admin console docker image
~~~

Example command to bring up the application:
~~~
docker-compose --env-file path/to/env/file/.env up
~~~

## Release notes
See [NOTES][NOTES] for latest.

[NOTES]: templates/NOTES.txt

### Older release notes collated below:

V Up to 1.12.3
- Various improvements to:
    - container technology
    - use of base-images
    - GITLAB Security scan framework implemented
    - Improved Open Source build process and container releases
    - Test improvements via docker-compose
    - Dependency upgrades
