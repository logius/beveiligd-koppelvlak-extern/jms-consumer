package nl.logius.osdbk.exception;

public class ThrottlingException extends RuntimeException {
    
    public ThrottlingException(String errorMessage) {
        super(errorMessage);
    }
}
