package nl.logius.osdbk.exception;

public class JmsConsumerException extends RuntimeException {
    public JmsConsumerException(Throwable t) {
        super(t);
    }
}
