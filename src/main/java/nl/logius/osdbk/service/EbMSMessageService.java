package nl.logius.osdbk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import nl.clockwork.ebms.model.MessageRequest;
import nl.logius.osdbk.util.RestUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class EbMSMessageService {

    @Value("${ebms.message-service.url}")
    private String ebmsMessageServiceEndpoint;
    @Value("${ebms.message-service.username}")
    private String ebmsMessageServiceUser;
    @Value("${ebms.message-service.password}")
    private String ebmsMessageServicePassword;

    private final RestTemplate restTemplate;

    @Autowired
    public EbMSMessageService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String sendMessage(MessageRequest body) {

        var uri = UriComponentsBuilder.fromUriString(ebmsMessageServiceEndpoint)
                .build(true)
                .toUri();

        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsMessageServiceUser, ebmsMessageServicePassword);
        HttpEntity<MessageRequest> request = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
        return response.getBody();
    }

    public String resendMessage(String messageId) {

        var uri = UriComponentsBuilder.fromUriString(ebmsMessageServiceEndpoint + "/" + messageId)
                .build(true)
                .toUri();

        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsMessageServiceUser, ebmsMessageServicePassword);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, request, String.class);
        return response.getBody();
    }

}
