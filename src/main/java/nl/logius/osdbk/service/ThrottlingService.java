package nl.logius.osdbk.service;

import nl.logius.osdbk.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@Service
public class ThrottlingService {

    @Value("${throttling.url}")
    private String throttlingBaseUrl;
    @Value("${throttling.username}")
    private String throttlingServiceUser;
    @Value("${throttling.password}")
    private String throttlingServicePassword;

    private final RestTemplate restTemplate;

    @Autowired
    public ThrottlingService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public boolean messageCanBeSentForAfnemer(String afnemerOin) {
        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(throttlingServiceUser, throttlingServicePassword);
        HttpEntity<?> request = new HttpEntity<>(headers);
        
        ResponseEntity<Boolean> response =  restTemplate.exchange(throttlingBaseUrl + "/" + afnemerOin, HttpMethod.GET, request, Boolean.class);
        return response.getBody();
    }

}
