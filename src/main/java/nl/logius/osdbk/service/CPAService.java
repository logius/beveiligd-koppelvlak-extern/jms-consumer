package nl.logius.osdbk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import nl.logius.osdbk.util.CPASearchCriteria;
import nl.logius.osdbk.util.RestUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class CPAService {
    
    @Value("${cpa-service.url}")
    private String cpaServiceEndpoint;
    @Value("${cpa-service.username}")
    private String cpaServiceUser;
    @Value("${cpa-service.password}")
    private String cpaServicePassword;
    
    private final RestTemplate restTemplate;

    @Autowired
    public CPAService(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }
    
    public MultiValueMap<String, String> findCPA(CPASearchCriteria searchCriteria) {
        
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fromPartyId", searchCriteria.getSender());
        params.add("toPartyId", searchCriteria.getReceiver());
        params.add("action", searchCriteria.getAction());
        params.add("service", searchCriteria.getService());
        params.add("serviceExtension", searchCriteria.getServiceExtension());
        
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(cpaServiceEndpoint)
                .queryParams(params)
                .encode()
                .toUriString();
        
        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(cpaServiceUser, cpaServicePassword);
        HttpEntity<MultiValueMap> request = new HttpEntity<>(headers);
        
        ResponseEntity<MultiValueMap> response =  restTemplate.exchange(urlTemplate, HttpMethod.GET, request, MultiValueMap.class);
        return response.getBody();
    }
    
}
