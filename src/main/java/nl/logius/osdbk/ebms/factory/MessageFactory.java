package nl.logius.osdbk.ebms.factory;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.TextMessage;
import nl.clockwork.ebms.model.DataSource;
import nl.clockwork.ebms.model.MessageRequest;
import nl.clockwork.ebms.model.MessageRequestProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;


@Component
public class MessageFactory {

    public MessageRequest createMessage(TextMessage message, MultiValueMap<String, String> cpaParams) throws JMSException {
        MessageRequestProperties requestProperties = createMessageRequestProperties(message, cpaParams);

        DataSource datasource = new DataSource();
        datasource.setContent(message.getText().getBytes());
        datasource.setContentType("text/xml; charset=UTF-8");
        MessageRequest messageRequest = new MessageRequest();
        messageRequest.setProperties(requestProperties);
        messageRequest.getDataSources().add(datasource);

        return messageRequest;
    }


    private MessageRequestProperties createMessageRequestProperties(Message message, MultiValueMap<String, String> cpaParams) throws JMSException {
        MessageRequestProperties messageRequestProperties = new MessageRequestProperties();
        messageRequestProperties.setCpaId(String.valueOf(cpaParams.getFirst("cpaId")));
        messageRequestProperties.setMessageId(message.getStringProperty("x_aux_system_msg_id"));
        messageRequestProperties.setConversationId(message.getStringProperty("x_aux_process_instance_id"));
        messageRequestProperties.setAction(message.getStringProperty("x_aux_action"));
        messageRequestProperties.setService("urn:osb:services:" + String.valueOf(cpaParams.getFirst("service")));
        messageRequestProperties.setFromPartyId("urn:osb:oin:" + message.getStringProperty("x_aux_sender_id"));
        messageRequestProperties.setFromRole(String.valueOf(cpaParams.getFirst("fromPartyName")));
        messageRequestProperties.setToPartyId("urn:osb:oin:" + message.getStringProperty("x_aux_receiver_id"));
        messageRequestProperties.setToRole(String.valueOf(cpaParams.getFirst("toPartyName")));

        return messageRequestProperties;
    }
}
