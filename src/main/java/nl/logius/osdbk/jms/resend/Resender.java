package nl.logius.osdbk.jms.resend;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import nl.logius.osdbk.exception.JmsConsumerException;
import nl.logius.osdbk.exception.ThrottlingException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidationException;
import nl.logius.osdbk.service.EbMSMessageService;
import nl.logius.osdbk.service.ThrottlingService;
import nl.logius.osdbk.util.LoggingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;

import java.util.Objects;

@Service
public class Resender {

    private static final Logger LOGGER = LoggerFactory.getLogger(Resender.class);

    private final JmsTemplate jmsTemplate;
    private final EbMSMessageService messageService;
    private final ThrottlingService throttlingService;

    @Autowired
    public Resender(JmsTemplate jmsTemplate, EbMSMessageService messageService, ThrottlingService throttlingService) {
        this.jmsTemplate = jmsTemplate;
        this.messageService = messageService;
        this.throttlingService = throttlingService;
    }

    @Value("${throttling.enabled}")
    private boolean isThrottlingEnabled;

    @Retryable(
            value = {RestClientResponseException.class, ThrottlingException.class, ResourceAccessException.class},
            maxAttempts = Integer.MAX_VALUE,
            backoff = @Backoff(delay = 1000, maxDelay = 30000),
            listeners = "jmsRetryListener"
    )
    @Transactional
    public void resend(String queueName, String messageId) {

        try {
            Message message = jmsTemplate.receiveSelected(queueName, "messageId = '" + messageId + "'");
            MDC.setContextMap(LoggingUtils.getPropertyMapFromMessageToResend(message));

            if (isThrottlingEnabled && Objects.nonNull(message) && !throttlingService.messageCanBeSentForAfnemer(message.getStringProperty("toPartyId"))) {
                throw new ThrottlingException("Message can not be sent. Afnemer is throttled and has no sending capacity. "
                        + "Message rollbacked to queue");
            }

            LOGGER.info("Requesting EbMS Core to resend message");
            String newMessageId = messageService.resendMessage(messageId);
            LOGGER.info(String.format("Message will be resent by EbMS Core with messageId %s", newMessageId));
            MDC.clear();

        } catch (JMSException ex) {
            throw new JMSHeaderValidationException(ex);
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            if (ex.getRawStatusCode() == 404) { // EbMS Core throws a 404 in case the message is not found
                LOGGER.warn("Corresponding message not found by EbMS Core. Message Event is removed from queue");
                // TODO how do we handle messages that are not found? DLQ or discard?
            } else {
                throw new JmsConsumerException(ex);
            }
        } finally {
            MDC.clear();
        }
    }

}
