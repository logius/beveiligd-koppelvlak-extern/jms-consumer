package nl.logius.osdbk.jms.resend;

import jakarta.jms.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Enumeration;

@Service
public class ResendService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResendService.class);

    private final JmsTemplate jmsTemplate;
    private final Resender resender;

    @Autowired
    public ResendService(JmsTemplate jmsTemplate, Resender resender) {
        this.jmsTemplate = jmsTemplate;
        this.resender = resender;
    }

    public void resend(String queueName, String cpaId) {

        jmsTemplate.browseSelected(queueName, "cpaId = '" + cpaId + "'", (session, browser) -> {

            if (Collections.list(browser.getEnumeration()).isEmpty()) {
                LOGGER.info(String.format("No messages found to resend for CPA %s", cpaId));
            } else {
                LOGGER.info(String.format("Found %d messages to resend for CPA %s", Collections.list(browser.getEnumeration()).size(), cpaId));
            }

            final Enumeration<Message> messagesForCPA = browser.getEnumeration();
            while (messagesForCPA.hasMoreElements()) {
                String messageId = messagesForCPA.nextElement().getStringProperty("messageId");
                resender.resend(queueName, messageId);
            }

            return 0; // the lambda expects a return value
        });
    }

}
