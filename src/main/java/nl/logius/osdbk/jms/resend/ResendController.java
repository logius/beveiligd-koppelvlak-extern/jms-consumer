package nl.logius.osdbk.jms.resend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ResendController {

    private final ResendService resendService;

    @Autowired
    public ResendController(ResendService resendService) {
        this.resendService = resendService;
    }

    @Value("${resend.queue.failed}")
    private String failedQueueName;

    @Value("${resend.queue.expired}")
    private String expiredQueueName;

    @PutMapping("/resend/failed/{cpaId}")
    public void resendFailedMessagesForCPA(@PathVariable String cpaId) {
        if (cpaId.isBlank()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing Request Parameter {cpaId}");
        }

        resendService.resend(failedQueueName, cpaId);
    }

    @PutMapping("/resend/expired/{cpaId}")
    public void resendExpiredMessagesForCPA(@PathVariable String cpaId) {
        if (cpaId.isBlank()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing Request Parameter {cpaId}");
        }

        resendService.resend(expiredQueueName, cpaId);
    }
    
}
