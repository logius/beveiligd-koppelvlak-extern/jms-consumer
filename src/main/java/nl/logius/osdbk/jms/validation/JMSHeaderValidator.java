package nl.logius.osdbk.jms.validation;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JMSHeaderValidator {
    
    private final Logger logger = LoggerFactory.getLogger(JMSHeaderValidator.class);

    public void validate(Message message) throws JMSException, JMSHeaderValidationException {
        List<Validator> headerValidators = new ArrayList<>();
        for (JMSHeader jmsHeader : JMSHeader.values()) {
            String headerValue = message.getStringProperty(jmsHeader.name().toLowerCase());

            logger.debug(String.format("%s: %s ", message.getStringProperty(jmsHeader.name().toLowerCase()), headerValue));

            Validator headerValidator = new Validator(jmsHeader.name(), headerValue, jmsHeader.getMaxLength());
            // only collect validators that validate to false
            if (!headerValidator.isValid()) {
                headerValidators.add(headerValidator);
            }

            if (!headerValidators.isEmpty()) {
                headerValidators.forEach(validator -> logger.warn(validator.toString()));
                throw new JMSHeaderValidationException("Error Code JMS01: Message " + message.getJMSMessageID()
                        + " contains at least one invalid header.");
            } else {
                logger.debug("Successfully validated all JMS-headers");
            }
        }
    }

}
