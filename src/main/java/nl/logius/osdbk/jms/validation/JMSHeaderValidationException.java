package nl.logius.osdbk.jms.validation;

public class JMSHeaderValidationException extends RuntimeException {

    public JMSHeaderValidationException(String message) {
        super(message);
    }

    public JMSHeaderValidationException(Exception e) {
        super(e);
    }

}
