package nl.logius.osdbk.jms.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;
import org.springframework.stereotype.Component;

@Component
public class JmsRetryListener extends RetryListenerSupport {
    private Logger logger = LoggerFactory.getLogger(JmsRetryListener.class);

    @Override
    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        logger.error("Exception occurred retries={} with reason={}", context.getRetryCount(), throwable.toString());
    }

}
