package nl.logius.osdbk.jms.configuration;

import jakarta.jms.ConnectionFactory;
import nl.logius.osdbk.jms.JmsMessageConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@ConditionalOnProperty(value = "jms-consumer.enabled", havingValue = "true", matchIfMissing = true)
@EnableTransactionManagement
public class JmsListenerConfiguration implements JmsListenerConfigurer {

    private final JmsConsumerConfiguration configuration;
    private final JmsMessageConsumer consumer;

    @Autowired
    public JmsListenerConfiguration(JmsConsumerConfiguration configuration, JmsMessageConsumer consumer) {
       this.configuration = configuration;
       this.consumer = consumer;
    }

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
        configuration.getListeners().forEach(listener -> {
            SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
            endpoint.setId(listener.getQueueName());
            endpoint.setDestination(listener.getQueueName());
            endpoint.setConcurrency(listener.getConcurrency());
            endpoint.setMessageListener(consumer);
            if (listener.getOinFilter() != null && !listener.getOinFilter().isBlank()) {
                endpoint.setSelector(buildJmsMessageSelector(listener.getOinFilter()));
            }
            registrar.registerEndpoint(endpoint);
        });
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(@Qualifier("jmsConnectionFactory") ConnectionFactory connectionFactory, JmsListenerErrorHandler errorHandler) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setErrorHandler(errorHandler);
        factory.setSessionTransacted(true);
        return factory;
    }

    private String buildJmsMessageSelector(String oinFilter) {

        return Stream.of(oinFilter.split(",", -1))
                .collect(Collectors.toList())
                .stream()
                .map(e -> "x_aux_receiver_id <> '" + e + "'")
                .collect(Collectors.joining(" AND "));
    }

    @Bean
    public JmsTemplate jmsTemplate(@Qualifier("jmsConnectionFactory") ConnectionFactory connectionFactory) {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        jmsTemplate.setSessionTransacted(true);
        return jmsTemplate;
    }
    
    @Bean
    public JmsTransactionManager jmsTransactionManager(@Qualifier("jmsConnectionFactory") ConnectionFactory connectionFactory) {
        JmsTransactionManager jmsTransactionManager = new JmsTransactionManager();
        jmsTransactionManager.setConnectionFactory(connectionFactory);
        return jmsTransactionManager;
    }

}
