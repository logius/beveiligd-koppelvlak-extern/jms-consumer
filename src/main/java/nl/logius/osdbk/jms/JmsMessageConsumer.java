package nl.logius.osdbk.jms;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;
import jakarta.jms.TextMessage;
import nl.clockwork.ebms.model.MessageRequest;
import nl.logius.osdbk.ebms.factory.MessageFactory;
import nl.logius.osdbk.exception.JmsConsumerException;
import nl.logius.osdbk.exception.ThrottlingException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidationException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidator;
import nl.logius.osdbk.service.CPAService;
import nl.logius.osdbk.service.EbMSMessageService;
import nl.logius.osdbk.service.ThrottlingService;
import nl.logius.osdbk.util.CPASearchCriteria;
import nl.logius.osdbk.util.LoggingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;

@Component
public class JmsMessageConsumer implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsMessageConsumer.class);

    private final EbMSMessageService messageService;
    private final JMSHeaderValidator headerValidator;
    private final MessageFactory messageFactory;
    private final ThrottlingService throttlingService;
    private final CPAService cpaService;

    @Autowired
    public JmsMessageConsumer(EbMSMessageService messageService, JMSHeaderValidator headerValidator,
                              MessageFactory messageFactory, ThrottlingService throttlingService, CPAService cpaService) {
       this.messageService = messageService;
       this.headerValidator = headerValidator;
       this.messageFactory = messageFactory;
       this.throttlingService = throttlingService;
       this.cpaService = cpaService;
    }

    @Value("${throttling.enabled}")
    private boolean isThrottlingEnabled;

    @Override
    @Retryable(
            value = {ThrottlingException.class, ResourceAccessException.class},
            maxAttempts = Integer.MAX_VALUE,
            backoff = @Backoff(delay = 1000, maxDelay = 30000),
            listeners = "jmsRetryListener"
    )
    @Transactional
    public void onMessage(Message message) {

        try {
            if (!(message instanceof TextMessage)) {
                throw new IllegalArgumentException("JMS Message not supported: " + message.getClass());
            }

            headerValidator.validate(message);

            CPASearchCriteria searchCriteria = buildSearchCriteria((TextMessage) message);
            if (isThrottlingEnabled && !throttlingService.messageCanBeSentForAfnemer(searchCriteria.getReceiver())) {
                throw new ThrottlingException("Message can not be sent. Afnemer " + searchCriteria.getReceiver()
                        + " is throttled and has no sending capacity. Message rollbacked to queue");
            }

            MultiValueMap<String, String> cpaParams = cpaService.findCPA(searchCriteria);
            MessageRequest requestMessage = messageFactory.createMessage((TextMessage) message, cpaParams);

            MDC.setContextMap(LoggingUtils.getPropertyMap(requestMessage.getProperties()));

            LOGGER.info("Sending message");
            messageService.sendMessage(requestMessage);

        } catch (JMSException jmse) {
            throw new JMSHeaderValidationException(jmse);
        } catch (RestClientResponseException e) {
            if (isDuplicateKeyException(e)) { // EbMS Core throws a DuplicateKeyException if the messageId already exists
                LOGGER.warn("Message was already processed. This could be due to a JMS redelivery");
            } else {
                throw new JmsConsumerException(e);
            }

        } finally {
            MDC.clear();
        }
    }

    private boolean isDuplicateKeyException(RestClientResponseException e) {
        String message = e.getMessage();
        return (message != null && message.contains("org.springframework.dao.DuplicateKeyException")) && message.contains("ebms_message_pkey");
    }

    private CPASearchCriteria buildSearchCriteria(TextMessage message) throws JMSException {
        String sender = message.getStringProperty("x_aux_sender_id");
        String receiver = message.getStringProperty("x_aux_receiver_id");
        String action = message.getStringProperty("x_aux_action");
        String service = message.getStringProperty("x_aux_process_type");
        String serviceExtension = "$" + message.getStringProperty("x_aux_process_version");

        return new CPASearchCriteria(sender, receiver, action, service, serviceExtension);
    }

}
