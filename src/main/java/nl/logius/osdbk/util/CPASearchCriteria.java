package nl.logius.osdbk.util;

public class CPASearchCriteria {
    private final String sender;
    private final String receiver;
    private final String action;
    private final String service;
    private final String serviceExtension;

    public CPASearchCriteria(String sender, String receiver, String action, String service, String serviceExtension) {
        this.sender = sender;
        this.receiver = receiver;
        this.action = action;
        this.service = service;
        this.serviceExtension = serviceExtension;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getAction() {
        return action;
    }

    public String getService() {
        return service;
    }

    public String getServiceExtension() {
        return serviceExtension;
    }
    
    @Override
    public String toString() {
        return "CPASearchCriteria: " + sender + " - " + receiver + " - " + action + " - " + service + " - " + serviceExtension;
    }
}
