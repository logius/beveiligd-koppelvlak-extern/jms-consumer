package nl.logius.osdbk.util;

import org.springframework.util.StringUtils;


public class EbMSUtils {

    private EbMSUtils() {
    }

    private static final String OIN_PREFIX = "urn:osb:oin:";

    public static String stripUrnPrefixes(String text) {
        text = StringUtils.delete(text, "urn:osb:services:");
        text = StringUtils.delete(text, OIN_PREFIX);
        return text;
    }

}
