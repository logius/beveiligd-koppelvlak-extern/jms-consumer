package nl.logius.osdbk.util;

import org.springframework.http.HttpHeaders;

public class RestUtils {

    private RestUtils() {
    }

    public static HttpHeaders createHeadersWithBasicAuth(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(username, password);
        return headers;
    }
}
