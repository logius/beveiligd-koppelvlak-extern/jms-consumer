package nl.logius.osdbk.util;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import nl.clockwork.ebms.model.MessageRequestProperties;

import java.util.HashMap;
import java.util.Map;

public class LoggingUtils {

    private LoggingUtils() {
    }

    public static Map<String, String> getPropertyMap(MessageRequestProperties requestProperties, Map<String, String> additionalProperties) {
        Map<String, String> properties = new HashMap<>();
        properties.put("messageId", requestProperties.getMessageId());
        properties.put("conversationId", requestProperties.getConversationId());
        properties.put("cpaId", requestProperties.getCpaId());
        properties.put("sender", EbMSUtils.stripUrnPrefixes(requestProperties.getFromPartyId()));
        properties.put("senderName", requestProperties.getFromRole());
        properties.put("receiver", EbMSUtils.stripUrnPrefixes(requestProperties.getToPartyId()));
        properties.put("receiverName", requestProperties.getToRole());
        properties.put("action", requestProperties.getAction());

        properties.putAll(additionalProperties);

        return properties;
    }

    public static Map<String, String> getPropertyMap(MessageRequestProperties requestProperties) {
        return getPropertyMap(requestProperties, new HashMap<>(4, 1.0f));
    }

    public static Map<String, String> getPropertyMapFromMessageToResend(Message message, Map<String, String> additionalProperties) throws JMSException {
        Map<String, String> properties = new HashMap<>();
        properties.put("originalMessageId", message.getStringProperty("messageId"));
        properties.put("conversationId", message.getStringProperty("conversationId"));
        properties.put("cpaId", message.getStringProperty("cpaId"));
        properties.put("sender", message.getStringProperty("fromPartyId"));
        properties.put("senderName", message.getStringProperty("fromRole"));
        properties.put("receiver", message.getStringProperty("toPartyId"));
        properties.put("receiverName", message.getStringProperty("toRole"));
        properties.put("action", message.getStringProperty("action"));

        properties.putAll(additionalProperties);

        return properties;
    }

    public static Map<String, String> getPropertyMapFromMessageToResend(Message message) throws JMSException {
        return LoggingUtils.getPropertyMapFromMessageToResend(message, new HashMap<>(4, 1.0f));
    }

}
