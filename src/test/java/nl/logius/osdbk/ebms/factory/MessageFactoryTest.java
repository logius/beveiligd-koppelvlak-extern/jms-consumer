package nl.logius.osdbk.ebms.factory;

import jakarta.jms.JMSException;
import jakarta.jms.TextMessage;
import nl.clockwork.ebms.model.MessageRequest;
import nl.clockwork.ebms.model.MessageRequestProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageFactoryTest {

    @InjectMocks
    private MessageFactory messageFactory;

    @Mock
    private TextMessage message;
    
    private MultiValueMap<String, String> cpaParams;
    private final String body = "body";

    @BeforeEach
    void setup() throws JMSException {
        when(message.getStringProperty("x_aux_system_msg_id")).thenReturn("msgId");
        when(message.getStringProperty("x_aux_process_instance_id")).thenReturn("convId");
        when(message.getStringProperty("x_aux_sender_id")).thenReturn("00000004003214345001");
        when(message.getStringProperty("x_aux_receiver_id")).thenReturn("22222222222222222222");
        when(message.getStringProperty("x_aux_action")).thenReturn("action");
        when(message.getText()).thenReturn(body);
        
        cpaParams = new LinkedMultiValueMap<>();
        cpaParams.add("cpaId", "cpaId");
        cpaParams.add("fromPartyName", "senderName");
        cpaParams.add("toPartyName", "receiverName");
        cpaParams.add("service", "service");
    }


    @Test
    public void testCreateMessage() throws JMSException{
        
       MessageRequest messageRequest = messageFactory.createMessage(message, cpaParams);
       MessageRequestProperties messageProperties = messageRequest.getProperties();
       
       
       assertEquals("text/xml; charset=UTF-8", messageRequest.getDataSources().get(0).getContentType());
       
       assertEquals("cpaId", messageProperties.getCpaId());
       assertEquals("senderName", messageProperties.getFromRole());
       assertEquals("receiverName", messageProperties.getToRole());
       assertEquals("urn:osb:oin:" + "00000004003214345001", messageProperties.getFromPartyId());
       assertEquals("urn:osb:oin:" + "22222222222222222222", messageProperties.getToPartyId());
       assertEquals("urn:osb:services:" + "service", messageProperties.getService());
       assertEquals("action", messageProperties.getAction());
       assertEquals("msgId", messageProperties.getMessageId());
       assertEquals("convId", messageProperties.getConversationId());
    }
}