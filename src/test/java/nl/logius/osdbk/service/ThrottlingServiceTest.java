package nl.logius.osdbk.service;

import nl.logius.osdbk.exception.ThrottlingException;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class ThrottlingServiceTest {
    
    @InjectMocks
    private ThrottlingService service;
    
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private ResponseEntity<Boolean> responseMock;
    
    private final String oin = "11111111111111111111";
    private final String url = "http://localhost:8080/throttling";
    private final String username = "user";
    private final String password = "pw";

    
    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(service, "throttlingBaseUrl", url);
        ReflectionTestUtils.setField(service, "throttlingServiceUser", username);
        ReflectionTestUtils.setField(service, "throttlingServicePassword", password);
        
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseMock);
    }
    
    @Test
    void throttleOrScheduleForSend() throws ThrottlingException {
        when(responseMock.getBody()).thenReturn(true);
        
        boolean canMessageBeSend = service.messageCanBeSentForAfnemer(oin);
        
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        assertTrue(canMessageBeSend);
    }
    
    @Test
    void throttleOrScheduleForSendToBeThrottled() throws ThrottlingException {
        when(responseMock.getBody()).thenReturn(false);
        
        boolean canMessageBeSend = service.messageCanBeSentForAfnemer(oin);
        
        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        assertFalse(canMessageBeSend);
    }
}
