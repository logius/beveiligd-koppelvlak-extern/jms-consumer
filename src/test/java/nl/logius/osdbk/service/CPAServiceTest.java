package nl.logius.osdbk.service;

import jakarta.xml.bind.JAXBException;
import nl.logius.osdbk.util.CPASearchCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class CPAServiceTest {

    @InjectMocks
    private CPAService service;

    @Mock
    private RestTemplate restTemplate;

    private final String url = "http://localhost:9090/cpa/find";
    private final String username = "user";
    private final String password = "pw";

    @BeforeEach
    public void setup() throws IOException, JAXBException {
        ReflectionTestUtils.setField(service, "cpaServiceEndpoint", url);
        ReflectionTestUtils.setField(service, "cpaServiceUser", username);
        ReflectionTestUtils.setField(service, "cpaServicePassword", password);
    }

    @Test
    void findCPA() {
        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");
        
        assertEquals("CPASearchCriteria: 00000004003214345001 - 22222222222222222222 - verstrekkingAanAfnemer - dgl:ontvangen:1.0 - $1.0", searchCriteria.toString());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fromPartyId", searchCriteria.getSender());
        params.add("toPartyId", searchCriteria.getReceiver());
        params.add("action", searchCriteria.getAction());
        params.add("service", searchCriteria.getService());
        params.add("serviceExtension", searchCriteria.getServiceExtension());
        

        ResponseEntity<MultiValueMap> responseMock = mock(ResponseEntity.class);
        when(responseMock.getBody()).thenReturn(params);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseMock);

        MultiValueMap responseParams = service.findCPA(searchCriteria);

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        assertEquals(responseParams.getFirst("action"), "verstrekkingAanAfnemer");
    }

}
