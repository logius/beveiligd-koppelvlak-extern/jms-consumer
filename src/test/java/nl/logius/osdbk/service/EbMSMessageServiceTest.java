package nl.logius.osdbk.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import nl.clockwork.ebms.model.MessageRequest;

import java.net.URI;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class EbMSMessageServiceTest {

    @InjectMocks
    private EbMSMessageService service;

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private MessageRequest body;
    @Mock
    private ResponseEntity<String> responseMock;

    private final String url = "http://localhost:8888/service/rest/v19/ebms/messages";
    private final String username = "user";
    private final String password = "pw";

    @BeforeEach
    void setup() {
        ReflectionTestUtils.setField(service, "ebmsMessageServiceEndpoint", url);
        ReflectionTestUtils.setField(service, "ebmsMessageServiceUser", username);
        ReflectionTestUtils.setField(service, "ebmsMessageServicePassword", password);
        
        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseMock);
    }

    @Test
    void sendMessage() {

        when(responseMock.getBody()).thenReturn("1234");

        String messageId = service.sendMessage(body);

        verify(restTemplate, times(1)).exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        assertEquals("1234", messageId);
    }

    @Test
    void resendMessage() {
        String messageId = "originalMessageId";
        String newMessageId = "newMessageId";
       
        when(responseMock.getBody()).thenReturn(newMessageId);

        String result = service.resendMessage(messageId);
        
        verify(restTemplate, times(1)).exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));
        assertEquals(newMessageId, result);
    }
}
