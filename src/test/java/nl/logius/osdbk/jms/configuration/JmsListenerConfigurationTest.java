package nl.logius.osdbk.jms.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JmsListenerConfigurationTest {

    @Mock
    private JmsConsumerConfiguration consumerConfiguration;
    @InjectMocks
    private JmsListenerConfiguration listenerConfiguration;

    @BeforeEach
    public void setup() {
        List<JmsConsumerConfiguration.Listener> listeners = new ArrayList<>();
        JmsConsumerConfiguration.Listener listener = new JmsConsumerConfiguration.Listener();
        listener.setQueueName("queue-1");
        listener.setConcurrency("2-4");
        listeners.add(listener);

        when(consumerConfiguration.getListeners()).thenReturn(listeners);
    }

    @Test
    void configureJmsListeners() {
        JmsListenerEndpointRegistrar registrar = mock(JmsListenerEndpointRegistrar.class);
        listenerConfiguration.configureJmsListeners(registrar);
        verify(registrar, times(1)).registerEndpoint(any(SimpleJmsListenerEndpoint.class));
    }
}