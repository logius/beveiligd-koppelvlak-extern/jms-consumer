package nl.logius.osdbk.jms.resend;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.QueueBrowser;
import jakarta.jms.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;

import java.util.Enumeration;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ResendServiceTest {
    
    @InjectMocks
    private ResendService service;
    @Mock
    private JmsTemplate jmsTemplate;
    @Mock
    private Resender resender;
    @Mock
    private Session session;
    @Mock
    private QueueBrowser browser;
    @Mock
    private Message message;
    
    @BeforeEach
    public void setUp() throws JMSException {
        when(message.getStringProperty(anyString())).thenReturn("string");
        
        Enumeration<Message> mockEnum = Mockito.mock(Enumeration.class);
        when(mockEnum.hasMoreElements()).thenReturn(true);
        when(mockEnum.nextElement()).thenReturn(message);
        when(browser.getEnumeration()).thenReturn(mockEnum);
    }
    
    @Test
    public void testResend() {
        assertDoesNotThrow(() -> service.resend("queue", "cpaId"));
    }
    
}
