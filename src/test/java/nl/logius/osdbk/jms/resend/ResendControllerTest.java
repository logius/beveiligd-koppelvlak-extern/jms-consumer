package nl.logius.osdbk.jms.resend;

import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.server.ResponseStatusException;

@SpringBootTest
public class ResendControllerTest {

    @InjectMocks
    private ResendController controller;
    @Mock
    private ResendService service;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(controller, "failedQueueName", "FAILED");
        ReflectionTestUtils.setField(controller, "expiredQueueName", "EXPIRED");
    }

    @Test
    public void testResendFailed() {
        controller.resendFailedMessagesForCPA("1");
        verify(service, times(1)).resend("FAILED", "1");
    }

    @Test
    public void testResendFailedWithBlankCpaId() {
        assertThrows(ResponseStatusException.class,
                () -> {
                    controller.resendFailedMessagesForCPA(" ");
                });
    }

    @Test
    public void testResendExpired() {
        controller.resendExpiredMessagesForCPA("1");
        verify(service, times(1)).resend("EXPIRED", "1");
    }

    @Test
    public void testResendExpiredWithBlankCpaId() {
        assertThrows(ResponseStatusException.class,
                () -> {
                    controller.resendExpiredMessagesForCPA(" ");
                });
    }

}
