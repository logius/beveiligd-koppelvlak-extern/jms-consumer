package nl.logius.osdbk.jms.resend;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import nl.logius.osdbk.exception.JmsConsumerException;
import nl.logius.osdbk.exception.ThrottlingException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidationException;
import nl.logius.osdbk.service.EbMSMessageService;
import nl.logius.osdbk.service.ThrottlingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ResenderTest {

    @InjectMocks
    private Resender resender;
    @Mock
    private JmsTemplate jmsTemplate;
    @Mock
    private EbMSMessageService ebmsMessageService;
    @Mock
    private ThrottlingService throttlingService;
    @Mock
    private Message message;

    @BeforeEach
    public void setUp() throws JMSException {
        ReflectionTestUtils.setField(resender, "isThrottlingEnabled", false);
        when(throttlingService.messageCanBeSentForAfnemer(anyString())).thenReturn(true);
        when(message.getStringProperty(anyString())).thenReturn("parameter");
        when(jmsTemplate.receiveSelected(anyString(), anyString())).thenReturn(message);
    }

    @Test
    public void testResend() {
        resender.resend("queue", "id");
        verify(ebmsMessageService, times(1)).resendMessage("id");
    }

    @Test
    public void testResendWithThrottlingException() {
        ReflectionTestUtils.setField(resender, "isThrottlingEnabled", true);
        when(throttlingService.messageCanBeSentForAfnemer(anyString())).thenReturn(false);

        assertThrows(ThrottlingException.class,
                () -> {
                    resender.resend("queue", "id");
                });
    }

    @Test
    public void testResendWithJmsException() throws JMSException {
        doThrow(new JMSException("oops.")).when(message).getStringProperty(anyString());
        assertThrows(JMSHeaderValidationException.class,
                () -> {
                    resender.resend("queue", "id");
                });
    }

    @Test
    public void testResendWithHttpServerErrorException() throws JMSException {
        when(ebmsMessageService.resendMessage(anyString())).thenThrow(new HttpServerErrorException(HttpStatus.valueOf(500)));
        assertThrows(JmsConsumerException.class,
                () -> {
                    resender.resend("queue", "id");
                });
    }

    @Test
    public void testResendWithNoCorrespondingMessage() throws JMSException {
        when(ebmsMessageService.resendMessage(anyString())).thenThrow(new HttpClientErrorException(HttpStatus.valueOf(404)));
        resender.resend("queue", "id");
        verify(ebmsMessageService, times(1)).resendMessage("id");
    }

}
