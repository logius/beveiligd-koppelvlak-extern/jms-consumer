package nl.logius.osdbk.jms;

import jakarta.jms.JMSException;
import jakarta.jms.MapMessage;
import jakarta.jms.TextMessage;
import nl.clockwork.ebms.model.MessageRequest;
import nl.clockwork.ebms.model.MessageRequestProperties;
import nl.logius.osdbk.ebms.factory.MessageFactory;
import nl.logius.osdbk.exception.JmsConsumerException;
import nl.logius.osdbk.exception.ThrottlingException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidationException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidator;
import nl.logius.osdbk.service.CPAService;
import nl.logius.osdbk.service.EbMSMessageService;
import nl.logius.osdbk.service.ThrottlingService;
import nl.logius.osdbk.util.CPASearchCriteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientResponseException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class JmsMessageConsumerTest {

    @Mock
    private EbMSMessageService messageService;

    @Mock
    private CPAService cpaService;

    @Mock
    private JMSHeaderValidator headerValidator;

    @Mock
    private MessageFactory messageFactory;

    @InjectMocks
    private JmsMessageConsumer messageConsumer;

    @Mock
    private TextMessage message;
    
    @Mock
    private MapMessage wrongMessageType;

    @Mock
    private MessageRequest content;

    @Mock
    private MessageRequestProperties context;

    @Mock
    ThrottlingService throttlingService;

    @BeforeEach
    public void setup() throws JMSException {
        when(context.getFromPartyId()).thenReturn("fromParty");
        when(context.getFromRole()).thenReturn("fromRole");
        when(context.getToPartyId()).thenReturn("toParty");
        when(context.getToRole()).thenReturn("toRole");
        when(cpaService.findCPA(any(CPASearchCriteria.class))).thenReturn(new LinkedMultiValueMap<>());
        when(messageFactory.createMessage(any(TextMessage.class), any(LinkedMultiValueMap.class))).thenReturn(content);
        when(content.getProperties()).thenReturn(context);
        when(message.getStringProperty(anyString())).thenReturn("oin");
        when(throttlingService.messageCanBeSentForAfnemer(anyString())).thenReturn(true);

        ReflectionTestUtils.setField(messageConsumer, "isThrottlingEnabled", true);
    }

    @Test
    void onMessage() throws RestClientResponseException, ThrottlingException {

        messageConsumer.onMessage(message);
        verify(throttlingService, times(1)).messageCanBeSentForAfnemer(anyString());
        verify(messageService, times(1)).sendMessage(content);
    }

    @Test
    void onMessageJMSExceptionInValidation() throws JMSException {
        doThrow(new JMSException("oops.")).when(headerValidator).validate(message);

        Assertions.assertThrows(JMSHeaderValidationException.class, () -> messageConsumer.onMessage(message));
    }

    @Test
    void onMessageDuplicateException() throws RestClientResponseException, ThrottlingException {
        String duplicateErrorMessage = "org.springframework.dao.DuplicateKeyException: PreparedStatementCallback; ERROR: duplicate key value violates unique constraint \"ebms_message_pkey\"";

        when(messageService.sendMessage(content)).thenThrow(new RestClientResponseException(duplicateErrorMessage, 404, duplicateErrorMessage, null,null,null));

        messageConsumer.onMessage(message); // ignores duplicate
        verify(throttlingService, times(1)).messageCanBeSentForAfnemer(anyString());
        verify(messageService, times(1)).sendMessage(content);
    }

    @Test
    void onMessageException() throws RestClientResponseException, ThrottlingException {
        String errorMessage = "oops.";

        when(messageService.sendMessage(content)).thenThrow(new RestClientResponseException(errorMessage, 404, errorMessage, null,null,null));

        Assertions.assertThrows(JmsConsumerException.class, () -> messageConsumer.onMessage(message));
    }

    @Test
    void onMessageThrottlingException() throws RestClientResponseException, ThrottlingException {

        when(throttlingService.messageCanBeSentForAfnemer(anyString())).thenReturn(false);

        Assertions.assertThrows(ThrottlingException.class, () -> messageConsumer.onMessage(message));
    }

    @Test
    void onMessageWrongMessageType() throws IllegalArgumentException {

        Assertions.assertThrows(IllegalArgumentException.class, () -> messageConsumer.onMessage(wrongMessageType));
    }
}
