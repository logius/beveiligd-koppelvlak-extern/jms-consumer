package nl.logius.osdbk.jms.validation;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { JMSHeaderValidatorTest.ThisTestConfiguration.class })
public class JMSHeaderValidatorTest {

    @ClassRule
    public static GenericContainer<?> activeMqContainer = new GenericContainer<>(DockerImageName.parse("rmohr/activemq:5.14.3")).withExposedPorts(61616);

    @Autowired
    private JmsTemplate jmsTemplate;

    @InjectMocks
    private JMSHeaderValidator headerValidator;

    @Test
    public void validate() throws JMSException {
        headerValidator.validate(createTextMessage());
    }

    @Test
    public void validateInvalidHeader() throws JMSException {
        TextMessage message = createTextMessage();
        message.setStringProperty("x_aux_sender_id", "0000000400321434500112345"); // OIN of 25 chars

        Assertions.assertThrows(JMSHeaderValidationException.class, () -> {
            headerValidator.validate(message);
        });
    }

    @Test
    public void validateEmptyHeader() throws JMSException {
        TextMessage message = createTextMessage();
        message.setStringProperty("x_aux_sender_id", ""); // OIN of 25 chars

        Assertions.assertThrows(JMSHeaderValidationException.class, () -> {
            headerValidator.validate(message);
        });
    }

    private TextMessage createTextMessage() throws JMSException {
        TextMessage msg = jmsTemplate.getConnectionFactory().createConnection().createSession().createTextMessage("test1234");
        msg.setJMSMessageID("fakeMessageId");
        msg.setStringProperty("x_aux_system_msg_id", "1234");
        msg.setStringProperty("x_aux_action", "verstrekkingAanAfnemer");
        msg.setStringProperty("x_aux_process_instance_id", "conv-id");
        msg.setStringProperty("x_aux_process_type", "dgl:ontvangen:1.0");
        msg.setStringProperty("x_aux_process_version", "1.0");
        msg.setStringProperty("x_aux_sender_id", "00000004003214345001");
        msg.setStringProperty("x_aux_receiver_id", "22222222222222222222");
        msg.setStringProperty("x_aux_activity", "dgl:ontvangen:1.0");
        msg.setStringProperty("x_aux_production", "Production");
        msg.setStringProperty("x_aux_protocol", "ebMS");
        msg.setStringProperty("x_aux_protocol_version", "1.0");
        msg.setStringProperty("x_aux_process_instance_id", "a682c271-ba49-4644-a547-bf07b4692ce7");
        msg.setStringProperty("x_aux_seq_number", "0");
        msg.setStringProperty("x_aux_msg_order", "TRUE");

        return msg;
    }

    @Configuration
    @EnableJms
    static class ThisTestConfiguration {
        @Bean
        public JmsListenerContainerFactory<?> jmsListenerContainerFactory() {
            DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
            factory.setConnectionFactory(connectionFactory());
            return factory;
        }

        @Bean
        public ConnectionFactory connectionFactory() {
            String brokerUrlFormat = "tcp://%s:%d";
            String brokerUrl = String.format(brokerUrlFormat, activeMqContainer.getHost(), activeMqContainer.getFirstMappedPort());
            return new ActiveMQConnectionFactory(brokerUrl);
        }

        @Bean
        public JmsTemplate jmsTemplate() {
            return new JmsTemplate(connectionFactory());
        }
    }

}
